const Sequelize = require("sequelize");
const db = require("../util/database");

const Ticker = db.define('ticker', {
  name: {
    type: Sequelize.STRING,
  },
  last: {
    type: Sequelize.STRING,
  },
  buy: {
    type: Sequelize.STRING,
  },
  sell: {
    type: Sequelize.STRING,
  },
  volume: {
    type: Sequelize.STRING,
  },
  base_unit: {
    type: Sequelize.STRING,
  },
});

module.exports = Ticker;
