// Imports
const express = require('express');
const fetch = require('node-fetch');
const Sequelize = require("sequelize");

const app = express();
const port = 5000;

const db = require('./util/database');
const Ticker = require('./models/Ticker');
//test db
db.authenticate()
.then(() => console.log('Database connected'))
.catch((err) => console.log(err));

// Static Files
app.use(express.static('public'));
var result = [];
let i = 0;
var obj2;

//Set view engine to ejs
app.set("view engine", "ejs"); 

//Tell Express where we keep our index.ejs
app.set("views", __dirname + "/views"); 
fetch('https://api.wazirx.com/api/v2/tickers')
.then(res => res.json())
.then(json => {
    var obj = JSON.stringify(json);
    obj2 = JSON.parse(obj);
    // convert object to key's array
    const keys = Object.keys(obj2);
    // console.log(keys[0]);
    for(i=0;i<10;i++) {
        result[i] = keys[i];
    }
    // console.log(result[0]);
    // console.log(obj2[result[0]]["name"]);
    // console.log(obj2["batwrx"]);


    //populating db ---------------------------------->
    // i = 0;
    // for(i=1;i<10;i++) {
    //     Ticker.create({
    //         name : obj2[result[i]]["name"],
    //         last : obj2[result[i]]["last"],
    //         buy : obj2[result[i]]["buy"],
    //         sell : obj2[result[i]]["sell"],
    //         volume : obj2[result[i]]["volume"],
    //         base_unit : obj2[result[i]]["base_unit"]
    //     })
    //     .then((ticker) => console.log(ticker))
    //     .catch((err) => console.log(err));
    // }
    //----------------------------------------------------->
    Ticker.findAll()
    .then((tickers) => {
        // console.log(tickers[0]["name"]);
        obj2 = tickers;
        console.log(typeof tickers);
    })
    .catch((err) => console.log(err));
})
.catch((err)=> console.log(err));


app.get('/', (req, res) => {
    res.render("main",{obj2 : obj2});
 })
//  app.get("/", function(req, res) {  
//     res.render("home", {name:'Chris Martin'});
//   });

// Listen on Port 5000
app.listen(port, () => console.log(`App listening on port ${port}`));